import xml.etree.ElementTree as ET


tree = ET.parse('primo.xml')
root = tree.getroot()

for neighbor in root.iter('error'):
    for key,val in neighbor.attrib.items():
        print("{} => {}".format(key,val))
    for loc in neighbor.attr.iter('location'):
        print(loc)
    input()

