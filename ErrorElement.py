import xml.etree.ElementTree as ET
from ErrorElement import *

class element:
    def __init__(self,id_, msg, sev, loc):
        self.messaggio = msg
        self.location1 = loc.pop(0)
        self.severity = sev
        self.id=id_
        
        try:
            l=loc.pop(0)
            if l is not None:
                self.location2 = l
        except:
            self.location2 = None

    def print_element(self):
        print("ELEMENT - ID: {}".format(self.id.get()))
        print("ELEMENT - MSG: {}".format(self.messaggio.get()))
        print("ELEMENT - LOC: {}".format(self.location1.get()))
        try:
            print("ELEMENT - LOC: {}".format( self.location2.get()))
        except:
            pass
        print("ELEMENT - SEVERITY:{}".format(self.severity.get()))
    
    def get_element():
        pass#return self.id.get(), self.severity.get(), self.messaggio.get(), self.location1.


class messaggio:
    def __init__(self, msg):
        self.mess=msg
        pass

    def to_string(self):
        print(self.mess)

    def get(self):
        return self.mess

class location:
    def __init__(self, l, f_):
        self.line = l
        self.file = f_
        pass

    def to_string(self):
        print("{} - {}".format(self.line, self.file))

    def get(self):
        return self.line, self.file

class severity:
    def __init__(self, s):
        self.severity = s
        pass
    
    def to_string(self):
        print(self.severity)

    def get(self):
        return self.severity

class type:
    def __init__(self, t):
        self.type = t

    def to_string(self):
        print(self.type)

    def get(self):
        return self.type

class element_id:
    def __init__(self, id_):
        self.id = id_
    
    def to_string(self):
        print(self.id)
    
    def get(self):
        return self.id
