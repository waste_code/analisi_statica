class ElementArray:
    def __init__ (self):
        self.element_array = []
        self.index=0

    def add_element(self, element):
        self.element_array.append(element)
    
    def get_element(self):
        e= self.element_array[self.index]
        self.index = self.index +1
        return e

    def add_element_array(self, elements):
        self.element_array = self.element_array + elements
    
    def get_whole_elements(self):
        return self.element_array
