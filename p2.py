import xml.etree.ElementTree as ET
from ErrorElement import *
from element_array import *


element_array = ElementArray()

tree = ET.parse('primo.xml')
root = tree.getroot()

for neighbor in root.iter('error'):
    m=messaggio(neighbor.attrib["msg"])
    #print("parsing - MESSAGGIO: {}".format(neighbor.attrib["msg"]))
    m.get()
    s=severity(neighbor.attrib["severity"])
    #print("parsing - SEVERITY: {}".format(neighbor.attrib["severity"]))
    s.get()
    el=element_id(neighbor.attrib["id"])
    #print("parsing - ID: {}".format(neighbor.attrib["id"]))
    loc = []
    #el.get_val()
    for a in neighbor.iter('location'):
        l=location(a.attrib["line"],a.attrib["file"])
        loc.append(l)
        #l.get_val()
    e=element(el,m,s,loc)
    #e.print_element()
    element_array.add_element(e)
    #input()

i=0
ele = element_array.get_whole_elements()
for e in ele:
    e.print_element()
    print("ITERATION {}".format(i))
    i+=1
    input()
